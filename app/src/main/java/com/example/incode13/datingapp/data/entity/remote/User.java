
package com.example.incode13.datingapp.data.entity.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("orientation")
    @Expose
    private String orientation;

    @SerializedName("password")
    @Expose
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();

        sb.append("****User****" + "\n");
        sb.append("username=").append(username).append('\n');
        sb.append("email=").append(email).append('\n');
        sb.append("birthday=").append(birthday).append('\n');
        sb.append("gender=").append(gender).append('\n');
        sb.append("orientation=").append(orientation).append('\n');
        sb.append("password=").append(password).append('\n');
        sb.append("*********");
        return sb.toString();
    }
}
