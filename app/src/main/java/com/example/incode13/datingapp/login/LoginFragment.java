package com.example.incode13.datingapp.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.data.entity.local.UserData;
import com.example.incode13.datingapp.resetpassword.ResetPasswordActivity;
import com.example.incode13.datingapp.common.BaseFragment;
import com.example.incode13.datingapp.utils.ErrorHelper;
import com.example.incode13.datingapp.utils.KeyBoardUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginFragment extends BaseFragment implements LoginContract.View {

    private static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.usernameWrapper)
    TextInputLayout mEmailWrapper;
    @BindView(R.id.passwordWrapper)
    TextInputLayout mPasswordWrapper;
    @BindView(R.id.username)
    EditText mEmailEditText;
    @BindView(R.id.password)
    EditText mPasswordEditText;
    @BindView(R.id.loginButton)
    Button mLoginButton;

    private LoginContract.Presenter mPresenter;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setProgressDialogMessage(getString(R.string.login_progress_dialog_message));
        mProgressDialog.setOnCancelListener(dialog -> mPresenter.cancelAuthentication());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, v);

        if (mPasswordWrapper.getEditText() != null) {
            mPasswordEditText.setOnTouchListener((view, motionEvent) -> {
                final int DRAWABLE_RIGHT = 2;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getX() >= (mPasswordEditText.getWidth() - mPasswordEditText
                            .getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        mPresenter.resetPassword(mEmailEditText.getText().toString());
                        return true;
                    }
                }
                return false;
            });
        }

        mLoginButton.setOnClickListener(view -> {
            KeyBoardUtils.hide(getActivity());
            mPresenter.login(getEmail(), getPassword());
        });

        return v;
    }

    private String getEmail() {
        return mEmailEditText.getText().toString().trim();
    }

    private String getPassword() {
        return mPasswordEditText.getText().toString().trim();
    }

    @Override
    public void showLoadingIndicator() {
        mProgressDialog.show();
    }

    @Override
    public void hideLoadingIndicator() {
        mProgressDialog.dismiss();
    }

    @Override
    public void showError(String errorMessage) {
        Snackbar.make(mLoginButton, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showInvalidUserDataError() {

    }

    @Override
    public void showBrowseProfiles(UserData userData) {
//        startActivity(MainActivity.newIntent(getActivity()));
    }

    @Override
    public void setUsernameError() {
        ErrorHelper.setError(mEmailWrapper, getString(R.string.error_incorrect_email_address));
    }

    @Override
    public void setPasswordError() {
        ErrorHelper.setError(mPasswordWrapper, getString(R.string.error_password_incorrect));
    }

    @Override
    public void removeErrors() {
        ErrorHelper.removeError(mEmailWrapper);
        ErrorHelper.removeError(mPasswordWrapper);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }

    @Override
    public void showResetPassword(String email) {
        Intent intent = ResetPasswordActivity.newIntent(getActivity(),
                email);
        startActivity(intent);
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }
}

