package com.example.incode13.datingapp.start;

import com.example.incode13.datingapp.utils.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(modules = StartModule.class)
public interface StartComponent {

    void inject(StartFragment fragment);
}
