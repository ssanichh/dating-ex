package com.example.incode13.datingapp.joininfo;

import com.example.incode13.datingapp.BasePresenter;
import com.example.incode13.datingapp.BaseView;

public interface JoinInfoContract {

    interface View extends BaseView {

        void showBrowseProfiles();

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showUserNameError();

        void showEmailError();

        void showBirthdayError();

        void showHeightError();

        void showEmptyPasswordError();

        void showShortPasswordError();

        void showWrongPasswordError();

        void showMatchPasswordsError();

        void showError(String message);

        void showDatePicker();

        void removeErrors();
    }

    interface Presenter extends BasePresenter {

        void cancelRegistration();

        void setDateOfBirth();

        void join(String username, String email, String birthday, String password,
                  String confirmPassword, String smocking, String alco, String drugs,
                  String edrucation, String bodyType, String religion, String height,
                  String additional, String orientation, String gender);
    }
}
