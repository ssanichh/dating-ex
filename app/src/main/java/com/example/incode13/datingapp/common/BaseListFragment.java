package com.example.incode13.datingapp.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.incode13.datingapp.R;

public abstract class BaseListFragment extends Fragment {

    protected abstract RecyclerView.Adapter<RecyclerView.ViewHolder> setupAdapter();

    protected RecyclerView.LayoutManager setupLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        return linearLayoutManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list, container, false);

        RecyclerView recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(setupLayoutManager());
        recyclerView.setAdapter(setupAdapter());
        return v;
    }
}
