package com.example.incode13.datingapp.start;

import android.os.Bundle;

import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.common.SingleFragmentActivity;
import com.example.incode13.datingapp.utils.ActivityUtils;

public class StartActivity extends SingleFragmentActivity {

    public static final String FLAG_IS_LOGIN = "islogin";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StartFragment fragment = (StartFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container);
        if (fragment == null) {
            fragment = StartFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.container);
        }

//            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//            boolean islogin = prefs.getBoolean(FLAG_IS_LOGIN, false);
//
//            if (islogin) {
//                nextActivity();
//            }
//
//            if (AccessToken.getCurrentAccessToken() != null) {
//                LoginManager.getInstance().logInWithReadPermissions(
//                        StartActivity.this,
//                        mPermissionNeeds);
//            }

//        new CheckUserProfile().execute();
//        File database = getApplicationContext().getDatabasePath("user.db");
//
//        if (!database.exists()) {
//            // Database does not exist so copy it from assets here
//            Log.i("Database", "Not Found");
//        } else {
//            nextActivity();
//            Log.i("Database", "Found");
//        }

//        Profile profile = Profile.getCurrentProfile();
//        if (profile != null) {
//            nextActivity();
//        } else {
//
//        }
//            Button signIn = (Button) findViewById(R.id.signInButton);
//            Button join = (Button) findViewById(R.id.joinButton);

//            signIn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(StartActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//            join.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(StartActivity.this, JoinGenderActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//

    }

//    private void nextActivity() {
//        Intent intent = new Intent(StartActivity.this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//    }
//
//    private class CheckUserProfile extends AsyncTask<Void, Void, Void> {
//        @Override
//        protected Void doInBackground(Void... voids) {
//            DatabaseHelper db = DatabaseHelper.getInstance(StartActivity.this);
//            Log.d("user", "doInBackground: " + db.getUserDetails().get(DatabaseHelperContract.UserRemoteData.KEY_NAME));
//            if (db.getUserDetails().get(DatabaseHelperContract.UserRemoteData.KEY_NAME) != null) {
//                nextActivity();
//            }
//            return null;
//        }
//    }
}
