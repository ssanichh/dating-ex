package com.example.incode13.datingapp.start;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

final class StartPresenter implements StartContract.Presenter {

    private final StartContract.View mStartView;

    @NonNull
    private CompositeSubscription mSubscriptions;

    @Inject
    StartPresenter(StartContract.View view){
        mStartView = view;
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void singIn() {
        mStartView.navigateToSingIn();
    }

    @Override
    public void signUp() {
        mStartView.navigateToSignUp();
    }

    @Override
    public void unsubscribe() {
        if (!mSubscriptions.isUnsubscribed()) {
            mSubscriptions.unsubscribe();
        }
    }
}
