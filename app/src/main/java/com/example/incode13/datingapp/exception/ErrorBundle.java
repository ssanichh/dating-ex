package com.example.incode13.datingapp.exception;

public interface ErrorBundle {

    Exception getException();

    String getErrorMessage();
}
