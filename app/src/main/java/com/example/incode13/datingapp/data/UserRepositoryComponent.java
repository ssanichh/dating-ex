package com.example.incode13.datingapp.data;

import com.example.incode13.datingapp.ApplicationModule;
import com.example.incode13.datingapp.data.rest.DatingApiModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {UserRepositoryModule.class, ApplicationModule.class, DatingApiModule.class})
public interface UserRepositoryComponent {

    UserRepository getUserRepository();
}
