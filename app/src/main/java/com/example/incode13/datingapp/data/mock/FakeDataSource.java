package com.example.incode13.datingapp.data.mock;

import android.support.annotation.NonNull;

import com.example.incode13.datingapp.data.UserDataSource;
import com.example.incode13.datingapp.data.cache.UserCache;
import com.example.incode13.datingapp.data.entity.local.UserData;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Response;
import rx.Observable;
import rx.functions.Action1;

public class FakeDataSource implements UserDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 2000;

    private UserCache mCache;

    public FakeDataSource (UserCache cache) {
        mCache = cache;
    }

    private final Action1<UserData> saveToCacheAction = userEntity -> {
        if (userEntity != null) {
            FakeDataSource.this.mCache.put(userEntity);
        }
    };

    @Override
    public Observable<UserData> getUserData(@NonNull String login, @NonNull String password) {
        UserData fakeUserData = new UserData();
        fakeUserData.setUsername("Fake UserRemoteData");
        fakeUserData.setEmail("fake@email.com");
        fakeUserData.setGender("Male");
        fakeUserData.setOrientation("S");
        fakeUserData.setPhotoUrl("https://goo.gl/images/b7B6rw");
        fakeUserData.setBirthday("2000-01-01");
        return Observable.just(fakeUserData)
                .delay(SERVICE_LATENCY_IN_MILLIS, TimeUnit.MILLISECONDS)
                .doOnNext(saveToCacheAction);
    }

    @Override
    public Observable<Response> resetPassword(String email) {
        return null;
    }

    @Override
    public Observable<UserData> register(UserData user) {
        return null;
    }

    @Override
    public Observable<List<UserData>> getUsers() {
        return null;
    }
}
