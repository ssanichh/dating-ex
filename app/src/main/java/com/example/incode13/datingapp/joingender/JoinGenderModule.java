package com.example.incode13.datingapp.joingender;

import dagger.Module;
import dagger.Provides;

@Module
public class JoinGenderModule {

    private final JoinGenderContract.View mView;

    public JoinGenderModule(JoinGenderContract.View view) {
        mView = view;
    }

    @Provides
    JoinGenderContract.View provideJoinGenderView() {
        return mView;
    }
}
