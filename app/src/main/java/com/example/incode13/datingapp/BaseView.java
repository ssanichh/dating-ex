package com.example.incode13.datingapp;

import android.content.Context;

public interface BaseView {

    Context context();
}
