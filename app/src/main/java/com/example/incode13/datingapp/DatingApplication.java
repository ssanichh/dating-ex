package com.example.incode13.datingapp;

import android.app.Application;

import com.example.incode13.datingapp.data.DaggerUserRepositoryComponent;
import com.example.incode13.datingapp.data.UserRepositoryComponent;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class DatingApplication extends Application {

    private static DatingApplication sSingleInstance = null;

    String mUsername;

    private UserRepositoryComponent mRepositoryComponent;

    public static DatingApplication getInstance()
    {
        return sSingleInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sSingleInstance = this;

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        mRepositoryComponent = DaggerUserRepositoryComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public UserRepositoryComponent getUserRepositoryComponent() {
        return mRepositoryComponent;
    }
}
