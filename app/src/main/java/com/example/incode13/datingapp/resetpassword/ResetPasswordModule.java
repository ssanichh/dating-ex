package com.example.incode13.datingapp.resetpassword;

import dagger.Module;
import dagger.Provides;

@Module
public class ResetPasswordModule {

    private final ResetPasswordContract.View mView;


    ResetPasswordModule(ResetPasswordContract.View view) {
        mView = view;
    }

    @Provides
    ResetPasswordContract.View provideResetPasswordContractView() {
        return mView;
    }
}
