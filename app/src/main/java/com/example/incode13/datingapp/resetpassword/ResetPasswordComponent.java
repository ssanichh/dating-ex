package com.example.incode13.datingapp.resetpassword;

import com.example.incode13.datingapp.data.UserRepositoryComponent;
import com.example.incode13.datingapp.utils.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = UserRepositoryComponent.class, modules = ResetPasswordModule.class)
public interface ResetPasswordComponent {

    void inject(ResetPasswordFragment fragment);
}
