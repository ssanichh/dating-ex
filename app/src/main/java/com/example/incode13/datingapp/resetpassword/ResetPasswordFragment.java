package com.example.incode13.datingapp.resetpassword;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.incode13.datingapp.DatingApplication;
import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.common.BaseFragment;
import com.example.incode13.datingapp.utils.ErrorHelper;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResetPasswordFragment extends BaseFragment implements ResetPasswordContract.View {

    private static final String ARG_EMAIL = "email";

    @BindView(R.id.emailWrapper)
    TextInputLayout mEmailWrapper;
    @BindView(R.id.email)
    EditText mEmailEditText;
    @BindView(R.id.resetButton)
    Button mResetButton;

    @Inject
    ResetPasswordPresenter mPresenter;

    private String mEmail;

    public static ResetPasswordFragment newInstance(String email) {
        Bundle args = new Bundle();
        args.putString(ARG_EMAIL, email);

        ResetPasswordFragment fragment = new ResetPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mEmail = getArguments().getString(ARG_EMAIL);

        DaggerResetPasswordComponent.builder()
                .userRepositoryComponent(((DatingApplication) getActivity()
                        .getApplication()).getUserRepositoryComponent())
                .resetPasswordModule(new ResetPasswordModule(this))
                .build()
                .inject(this);

        mProgressDialog.setMessage(getString(R.string.reset_password_progress_dialog_message));
        mProgressDialog.setOnCancelListener(dialogInterface -> mPresenter.cancelProgress());
    }

    private String getEmail() {
        return mEmailEditText.getText().toString().trim();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reset_password, container, false);
        ButterKnife.bind(this, v);

        if (mEmail != null) {
            mEmailEditText.setText(mEmail);
        }

        mResetButton.setOnClickListener(view -> mPresenter.resetPassword(getEmail()));

        return v;
    }

    @Override
    public void showLoadingIndicator() {
        mProgressDialog.show();
    }

    @Override
    public void hideLoadingIndicator() {
        mProgressDialog.dismiss();
    }

    @Override
    public void setEmailError(String message) {
        ErrorHelper.setError(mEmailWrapper, message);
    }

    @Override
    public void removeErrors() {
        ErrorHelper.removeError(mEmailWrapper);
    }

    @Override
    public void navigateToLogin() {
        getActivity().onBackPressed();
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }
}
