package com.example.incode13.datingapp.utils;

import android.support.design.widget.TextInputLayout;

public class ErrorHelper {

    public static void setError(TextInputLayout wrapper, String error) {
        wrapper.setErrorEnabled(true);
        wrapper.setError(error);
    }

    public static void removeError(TextInputLayout wrapper) {
        wrapper.setError(null);
        wrapper.setErrorEnabled(false);
    }
}
