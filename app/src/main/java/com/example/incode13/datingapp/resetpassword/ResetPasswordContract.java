package com.example.incode13.datingapp.resetpassword;

import com.example.incode13.datingapp.BasePresenter;
import com.example.incode13.datingapp.BaseView;

public interface ResetPasswordContract {

    interface View extends BaseView {

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void setEmailError(String message);

        void removeErrors();

        void navigateToLogin();
    }

    interface Presenter extends BasePresenter {

        void resetPassword(String email);

        void cancelProgress();
    }
}
