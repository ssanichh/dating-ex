package com.example.incode13.datingapp.data;

import android.content.Context;

import com.example.incode13.datingapp.data.auth.Auth;
import com.example.incode13.datingapp.data.cache.Cache;
import com.example.incode13.datingapp.data.cache.UserCache;
import com.example.incode13.datingapp.data.cache.serializer.JsonSerializer;
import com.example.incode13.datingapp.data.entity.mapper.UserDataMapper;
import com.example.incode13.datingapp.data.local.UserLocalDataSource;
import com.example.incode13.datingapp.data.remote.UserRemoteDataSource;
import com.example.incode13.datingapp.data.rest.DatingApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UserRepositoryModule {

    @Singleton
    @Provides
    UserCache provideUserCache(Context context, JsonSerializer serializer){
        return new Cache(context, serializer);
    }

    @Singleton
    @Remote
    @Provides
    UserDataSource provideUserRemoteDataSource(Context context, UserDataMapper mapper,
                                               UserCache cache, Auth auth, DatingApi service) {
        return new UserRemoteDataSource(context, mapper, cache, auth, service);
//        return new FakeDataSource(cache);
    }

    @Singleton
    @Local
    @Provides
    UserDataSource provideUserLocalDataSource(UserCache cache) {
        return new UserLocalDataSource(cache);
    }
}
