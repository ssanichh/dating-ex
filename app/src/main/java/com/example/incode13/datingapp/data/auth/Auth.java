package com.example.incode13.datingapp.data.auth;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Auth implements Oauth {

    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";

    private SharedPreferences mSharedPreferences;

    @Inject
    Auth(Context context) {
        initSharedPref(context);
    }

    private void initSharedPref(Context context) {
        mSharedPreferences = context.getSharedPreferences("oauth", Context.MODE_PRIVATE);
    }

    private SharedPreferences getOAuthCredentials() {
        return mSharedPreferences;
    }

    private SharedPreferences.Editor editSharedPrefs() {
        return getOAuthCredentials().edit();
    }

    @Override
    @Nullable
    public String get(String key) {
        return mSharedPreferences.getString(key, null);
    }

    public Long getLong(String key) {
        return mSharedPreferences.getLong(key, -1);
    }

    @Override
    public void set(String key, String value) {
        editSharedPrefs().putString(key, value).commit();
    }

    public void set(String key, Long value) {
        editSharedPrefs().putLong(key, value);
    }

    @Override
    public boolean isAuthorized() {
        return (get(ACCESS_TOKEN) != null);
    }

}
