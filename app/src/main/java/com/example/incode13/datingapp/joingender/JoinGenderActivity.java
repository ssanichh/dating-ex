package com.example.incode13.datingapp.joingender;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.common.ToolbarFragmentActivity;
import com.example.incode13.datingapp.utils.ActivityUtils;

public class JoinGenderActivity extends ToolbarFragmentActivity {


    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, JoinGenderActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JoinGenderFragment fragment = (JoinGenderFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container);
        if (fragment == null) {
            fragment = JoinGenderFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.container);
        }

        getSupportActionBar().setTitle(R.string.join_gender_toolbar_title);
    }
}
