package com.example.incode13.datingapp.joininfo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.example.incode13.datingapp.DatingApplication;
import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.common.BaseFragment;
import com.example.incode13.datingapp.utils.DatePickerFragment;
import com.example.incode13.datingapp.utils.KeyBoardUtils;

import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.incode13.datingapp.R.id.joinButton;
import static com.example.incode13.datingapp.utils.ErrorHelper.removeError;
import static com.example.incode13.datingapp.utils.ErrorHelper.setError;
import static com.example.incode13.datingapp.utils.SpinnerUtils.setupSpinner;

public class JoinInfoFragment extends BaseFragment implements View.OnClickListener,
         AdapterView.OnItemSelectedListener, JoinInfoContract.View {

    private static final String TAG = JoinInfoFragment.class.getSimpleName();
    private static final String ARG_GENDER = "gender";
    private static final String ARG_ORIENTATION = "orientation";
    private static final String DIALOG_DATE = "dialogDate";
    private static final int REQUEST_DATE = 0;

    @BindView(R.id.alco)
    Spinner mAlcoSpinner;
    @BindView(R.id.smocking)
    Spinner mSmockingSpinner;
    @BindView(R.id.grugs)
    Spinner mDrugsSpinner;
    @BindView(R.id.bodyType)
    Spinner mBodyTypeSpinner;
    @BindView(R.id.education)
    Spinner mEducationSpinner;
    @BindView(R.id.religion)
    Spinner mReligionSpinner;
    @BindView(R.id.usernameRegistrationWrapper)
    TextInputLayout mUsernameWrapper;
    @BindView(R.id.emailWrapper)
    TextInputLayout mEmailWrapper;
    @BindView(R.id.heightWrapper)
    TextInputLayout mHeightWrapper;
    @BindView(R.id.birthdayWrapper)
    TextInputLayout mBirthdayWrapper;
    @BindView(R.id.passwordWrapper)
    TextInputLayout mPasswordWrapper;
    @BindView(R.id.confirmPasswordWrapper)
    TextInputLayout mConfirmPasswordWrapper;
    @BindView(R.id.additionalWrapper)
    TextInputLayout mAdditionalWrapper;
    @BindView(R.id.username)
    EditText mUsernameEditText;
    @BindView(R.id.email)
    EditText mEmailEditText;
    @BindView(R.id.height)
    EditText mHeightEditText;
    @BindView(R.id.password)
    EditText mPasswordEditText;
    @BindView(R.id.confirmPassword)
    EditText mConfirmPasswordEditText;
    @BindView(R.id.birthday)
    EditText mBirthdayEditText;
    @BindView(R.id.additional)
    EditText mAdditionalEditText;
    @BindView(joinButton)
    Button mJoinButton;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;

    @Inject
    JoinInfoPresenter mPresenter;

    private String mGender;
    private String mOrientation;
    private String mUserName;
    private String mBirthday;
    private String mEmail;
    private String mAdditional;
    private String mPassword;
    private String mConfirmPassword;
    private String mAlco;
    private String mSmocking;
    private String mDrugs;
    private String mBodyType;
    private String mEducation;
    private String mReligion;
    private String mHeight;

    public static JoinInfoFragment newInstance(String gender, String orientation) {
        Bundle args = new Bundle();
        args.putString(ARG_GENDER, gender);
        args.putString(ARG_ORIENTATION, orientation);
        JoinInfoFragment fragment = new JoinInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGender = getArguments().getString(ARG_GENDER);
        mOrientation = getArguments().getString(ARG_ORIENTATION);

        DaggerJoinInfoComponent.builder()
                .userRepositoryComponent(((DatingApplication) getActivity()
                        .getApplication()).getUserRepositoryComponent())
                .joinInfoModule(new JoinInfoModule(this))
                .build()
                .inject(this);

        setProgressDialogMessage(getString(R.string.join_info_progress_message));
        mProgressDialog.setOnCancelListener(dialog -> mPresenter.cancelRegistration());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_join_info, container, false);
        ButterKnife.bind(this, v);

        mJoinButton.setOnClickListener(this);

        mBirthdayEditText.setKeyListener(null);
        mBirthdayEditText.setOnFocusChangeListener((view, b) -> {
            if (b) {
                KeyBoardUtils.hide(getActivity());
                mPresenter.setDateOfBirth();
            }
        });

        setupSpinner(mAlcoSpinner, R.array.attitude, this);
        setupSpinner(mSmockingSpinner, R.array.attitude, this);
        setupSpinner(mDrugsSpinner, R.array.attitude, this);
        setupSpinner(mBodyTypeSpinner, R.array.body_types, this);
        setupSpinner(mEducationSpinner, R.array.education, this);
        setupSpinner(mReligionSpinner, R.array.religion, this);

        return v;
    }

    @Override
    public void showDatePicker() {
        FragmentManager manager = getFragmentManager();
        DatePickerFragment dialog = DatePickerFragment
                .newInstance(new Date());
        dialog.setTargetFragment(JoinInfoFragment.this, REQUEST_DATE);
        dialog.show(manager, DIALOG_DATE);
    }

    @Override
    public void onClick(View view) {
        mUserName = mUsernameEditText.getText().toString();
        mEmail = mEmailEditText.getText().toString().trim();
        mHeight = mHeightEditText.getText().toString().trim();
        mPassword = mPasswordEditText.getText().toString().trim();
        mConfirmPassword = mConfirmPasswordEditText.getText().toString().trim();
        mBirthday = mBirthdayEditText.getText().toString().trim();
        mAdditional = mAdditionalEditText.getText().toString();

        mPresenter.join(mUserName, mEmail, mBirthday, mPassword, mConfirmPassword,
                mSmocking, mAlco, mDrugs, mEducation, mBodyType, mReligion, mHeight,
                mAdditional, mOrientation, mGender);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case (R.id.alco):
                mAlco = adapterView.getSelectedItem().toString();
                break;
            case (R.id.smocking):
                mSmocking = adapterView.getSelectedItem().toString();
                break;
            case (R.id.grugs):
                mDrugs = adapterView.getSelectedItem().toString();
                break;
            case (R.id.bodyType):
                mBodyType = adapterView.getSelectedItem().toString();
                break;
            case (R.id.education):
                mEducation = adapterView.getSelectedItem().toString();
                break;
            case (R.id.religion):
                mReligion = adapterView.getSelectedItem().toString();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_DATE) {
            String date = data
                    .getStringExtra(DatePickerFragment.EXTRA_DATE);

            mBirthdayEditText.setText(date);

            mHeightEditText.requestFocus();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void showBrowseProfiles() {
//        startActivity(MainActivity.newIntent(getActivity()));
    }

    @Override
    public void showLoadingIndicator() {
        mProgressDialog.show();
    }

    @Override
    public void hideLoadingIndicator() {
        mProgressDialog.dismiss();
    }

    @Override
    public void showUserNameError() {
        setError(mUsernameWrapper, getString(R.string.error_empty_username));
        scrollToField(mUsernameWrapper);
    }

    @Override
    public void showEmailError() {
        setError(mEmailWrapper, getString(R.string.error_invalid_email));
        scrollToField(mEmailWrapper);
    }

    @Override
    public void showBirthdayError() {
        setError(mBirthdayWrapper, getString(R.string.error_empty_birthday_title));
        scrollToField(mBirthdayWrapper);
    }

    @Override
    public void showHeightError() {
        setError(mHeightWrapper, getString(R.string.error_empty_field));
        scrollToField(mHeightWrapper);
    }

    private void scrollToField(TextInputLayout field) {
        mScrollView.smoothScrollTo(0, field.getScrollY());
        field.requestFocus();
    }

    @Override
    public void showEmptyPasswordError() {
        setError(mPasswordWrapper, getString(R.string.error_empty_password));
        scrollToField(mPasswordWrapper);
    }

    @Override
    public void showShortPasswordError() {
        setError(mPasswordWrapper, getString(R.string.error_too_short_password));
        scrollToField(mPasswordWrapper);
    }

    @Override
    public void showWrongPasswordError() {

    }

    @Override
    public void showMatchPasswordsError() {
        setError(mConfirmPasswordWrapper, getString(R.string.error_password_doesnt_match));
        scrollToField(mConfirmPasswordWrapper);
    }

    @Override
    public void showError(String message) {
        Snackbar.make(mJoinButton, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void removeErrors() {
        removeError(mUsernameWrapper);
        removeError(mEmailWrapper);
        removeError(mPasswordWrapper);
        removeError(mConfirmPasswordWrapper);
        removeError(mHeightWrapper);
        removeError(mBirthdayWrapper);
        removeError(mConfirmPasswordWrapper);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }
}
