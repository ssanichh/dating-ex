package com.example.incode13.datingapp.start;

import com.example.incode13.datingapp.BasePresenter;
import com.example.incode13.datingapp.BaseView;

public interface StartContract {

    interface View extends BaseView {

        void navigateToSingIn();

        void navigateToSignUp();
    }

    interface Presenter extends BasePresenter {

        void singIn();

        void signUp();
    }
}
