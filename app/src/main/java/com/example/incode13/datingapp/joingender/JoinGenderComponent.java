package com.example.incode13.datingapp.joingender;

import com.example.incode13.datingapp.utils.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(modules = JoinGenderModule.class)
public interface JoinGenderComponent {

    void inject(JoinGenderFragment fragment);
}
