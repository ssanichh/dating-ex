package com.example.incode13.datingapp.login;

import com.example.incode13.datingapp.data.UserRepositoryComponent;
import com.example.incode13.datingapp.utils.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = UserRepositoryComponent.class, modules = LoginModule.class)
public interface LoginComponent {

    void inject(LoginActivity activity);
}
