package com.example.incode13.datingapp;

public interface BasePresenter {

    void unsubscribe();
}
