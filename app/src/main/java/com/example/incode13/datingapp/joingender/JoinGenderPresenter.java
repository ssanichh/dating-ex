package com.example.incode13.datingapp.joingender;

import javax.inject.Inject;

public class JoinGenderPresenter implements JoinGenderContract.Presenter {

    private final JoinGenderContract.View mView;


    @Inject
    JoinGenderPresenter(JoinGenderContract.View view) {
        mView = view;
    }

    @Override
    public void continueRegistration() {
        mView.showJoinInfo();
    }
}
