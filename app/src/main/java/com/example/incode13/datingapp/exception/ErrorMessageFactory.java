package com.example.incode13.datingapp.exception;

import android.content.Context;

import com.example.incode13.datingapp.R;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;

public class ErrorMessageFactory {

    private static final String TAG = ErrorMessageFactory.class.getSimpleName();

    private ErrorMessageFactory() {}

    public static String create(Context context, Exception exception) {
        String message = context.getString(R.string.error_generic);

        if (exception instanceof ConnectException ||
                exception instanceof NetworkConnectionException) {
            message = context.getString(R.string.error_no_internet_connection);

        } else if (exception instanceof SocketTimeoutException) {
            message = context.getString(R.string.error_failed_connect_to_server);
        } else if (exception instanceof HttpException) {
            message = exception.getMessage();
        } else if (exception instanceof UserNotFoundException) {
            message = "User not found";
        }

        return message;
    }
}
