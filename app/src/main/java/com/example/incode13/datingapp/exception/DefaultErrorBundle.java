package com.example.incode13.datingapp.exception;

public class DefaultErrorBundle implements ErrorBundle {

    private static final String DEFAULT_ERROR_MSG = "Unknown error";

    private Exception mException;

    public DefaultErrorBundle(Exception e) {
        mException = e;
    }

    @Override
    public Exception getException() {
        return mException;
    }

    @Override
    public String getErrorMessage() {
        return mException != null ? mException.getMessage() : DEFAULT_ERROR_MSG;
    }
}
