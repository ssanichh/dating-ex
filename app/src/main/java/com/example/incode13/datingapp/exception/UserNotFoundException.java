package com.example.incode13.datingapp.exception;

public class UserNotFoundException extends Exception {

    public UserNotFoundException() {
        super();
    }
}
