package com.example.incode13.datingapp.data.auth;

public interface Oauth {

    void set(String key, String value);

    String get(String key);

    boolean isAuthorized();
}
