package com.example.incode13.datingapp.utils;

import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import java.util.regex.Pattern;

public class ValidatorUtil {

    private static final String TAG =  ValidatorUtil.class.getSimpleName();
    private static final String PATTERN_EMAIL = "^([\\w]+)(([-\\.\\+][\\w]+)?)*@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
    private static final String PATTERN_NAME = "^[а-яА-Яa-zA-Z-іІїЇґҐєЄ'`\\.0-9\\s]{2,32}$";
    private static final int MIN_TEXT_LENGTH = 5;

    private ValidatorUtil() {
    }

    public static boolean isEmail(final String email) {
        return email != null && Pattern.matches(PATTERN_EMAIL, email);
    }

    public static boolean isEmpty(final String... names) {
        if (names == null) {
            return true;
        }

        for (String name : names) {
            if (TextUtils.isEmpty(name)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidName(final String... names) {
        if (names == null) {
            return false;
        }

        for (String name : names) {
            if (TextUtils.isEmpty(name) || !Pattern.matches(PATTERN_NAME, name)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidEmail(final String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static boolean isValidUserName(final String username) {
        return isValidName(username) || isValidEmail(username);
    }

    public static boolean isShort(final String text) {
            return text.length() < MIN_TEXT_LENGTH;
    }

    public static boolean isEqual(String text1, String text2) {
        boolean b = text1.equals(text2);
        Log.i(TAG, "text1: " + text1 + "\n" +
        "text2: " + text2 + "\n" + "equal: " + b);
        return text1.equals(text2);
    }
}
