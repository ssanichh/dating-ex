package com.example.incode13.datingapp.data.cache.serializer;

import com.example.incode13.datingapp.data.entity.local.UserData;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class JsonSerializer {

    private final Gson gson = new Gson();

    @Inject
    public JsonSerializer() {}

    public String serialize(UserData userData) {
        return gson.toJson(userData, UserData.class);
    }

    public UserData deserialize(String jsonString) {
        return gson.fromJson(jsonString, UserData.class);
    }
}
