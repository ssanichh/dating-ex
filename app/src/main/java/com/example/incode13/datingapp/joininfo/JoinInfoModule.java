package com.example.incode13.datingapp.joininfo;

import dagger.Module;
import dagger.Provides;

@Module
public class JoinInfoModule {

    private final JoinInfoContract.View mView;

    public JoinInfoModule(JoinInfoContract.View view) {
        mView = view;
    }

    @Provides
    JoinInfoContract.View provideJoinInfoView() {
        return mView;
    }
}
