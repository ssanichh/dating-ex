package com.example.incode13.datingapp.login;

import com.example.incode13.datingapp.data.UserRepository;
import com.example.incode13.datingapp.data.entity.local.UserData;
import com.example.incode13.datingapp.exception.DefaultErrorBundle;
import com.example.incode13.datingapp.exception.ErrorBundle;
import com.example.incode13.datingapp.exception.ErrorMessageFactory;
import com.example.incode13.datingapp.utils.ValidatorUtil;

import javax.inject.Inject;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

final class LoginPresenter implements LoginContract.Presenter {

    private static final String TAG = LoginPresenter.class.getSimpleName();

    private final UserRepository mUserRepository;
    private final LoginContract.View mLoginView;

    private CompositeSubscription mSubscriptions;

    @Inject
    LoginPresenter(UserRepository userRepository, LoginContract.View loginView) {
        mUserRepository = userRepository;
        mLoginView = loginView;
        mSubscriptions = new CompositeSubscription();
    }

    @Inject
    void setupListeners() {
        mLoginView.setPresenter(this);
    }

    @Override
    public void login(String user, String password) {
        mLoginView.removeErrors();

        if (!ValidatorUtil.isValidUserName(user)) {
            mLoginView.setUsernameError();
        } else if (ValidatorUtil.isEmpty(password)) {
            mLoginView.setPasswordError();
        } else {
            loginUser(user, password);
        }
    }

    @Override
    public void cancelAuthentication() {
        unsubscribe();
        mLoginView.hideLoadingIndicator();
    }

    @Override
    public void resetPassword(String email) {
        if (ValidatorUtil.isValidEmail(email)) {
            mLoginView.showResetPassword(email);
        } else {
            mLoginView.showResetPassword(null);
        }
    }

    private void loginUser(String userName, String password) {
        mLoginView.showLoadingIndicator();

        Subscription subscription = mUserRepository
                .getUserData(userName, password)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserData>() {
                    @Override
                    public void onCompleted() {
                        mLoginView.hideLoadingIndicator();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mLoginView.hideLoadingIndicator();
                        showError(new DefaultErrorBundle((Exception) e));
                    }

                    @Override
                    public void onNext(UserData user) {
                        processUserData(user);
                    }
                });

        mSubscriptions.add(subscription);
    }

    private void showError(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(mLoginView.context(), errorBundle.getException());
        mLoginView.showError(errorMessage);
    }

    private void processUserData(UserData userData) {
        if (userData == null) {
            mLoginView.showInvalidUserDataError();
        } else {
            mLoginView.showBrowseProfiles(userData);
        }
    }

    @Override
    public void unsubscribe() {
        if (!mSubscriptions.isUnsubscribed()) {
            mSubscriptions.clear();
        }
    }
}
