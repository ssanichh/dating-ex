package com.example.incode13.datingapp.data;

import android.support.annotation.NonNull;

import com.example.incode13.datingapp.data.entity.local.UserData;

import java.util.List;

import okhttp3.Response;
import rx.Observable;

public interface UserDataSource {

    Observable<UserData> getUserData(@NonNull String login, @NonNull String password);

    Observable<Response> resetPassword(String email);

    Observable<UserData> register(UserData user);

    Observable<List<UserData>> getUsers();
}
