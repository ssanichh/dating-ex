package com.example.incode13.datingapp.common;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.incode13.datingapp.R;

public abstract class BaseTabFragment extends Fragment {

    protected TabLayout mTabLayout;
    protected ViewPager mViewPager;

    protected abstract FragmentPagerAdapter setupAdapter();

    protected abstract void setupTabs();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tab, container, false);

        mTabLayout = (TabLayout) v.findViewById(R.id.tabLayout);

        if (mTabLayout != null) {
            setupTabs();
            mTabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        }

        mViewPager = (ViewPager) v.findViewById(R.id.pager);

        if (mViewPager != null) {
            mViewPager.setAdapter(setupAdapter());
            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

            if (mTabLayout != null) {
                mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        mViewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                    }
                });
            }
        }
        return v;
    }
}
