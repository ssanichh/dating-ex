package com.example.incode13.datingapp.data.cache;

import android.content.Context;
import android.util.Log;

import com.example.incode13.datingapp.data.cache.serializer.JsonSerializer;
import com.example.incode13.datingapp.data.entity.local.UserData;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

@Singleton
public class Cache implements UserCache {

    private static final String TAG = Cache.class.getSimpleName();

    private Context mContext;
    private JsonSerializer mSerializer;

    @Inject
    public Cache(Context context, JsonSerializer serializer){
        mContext = context;
        mSerializer = serializer;
    }

    @Override
    public Observable<UserData> get() {
        return null;
    }

    @Override
    public void put(UserData userData) {
        Log.d(TAG, userData.toString());
    }

    @Override
    public boolean isCached() {
        return false;
    }

    @Override
    public boolean isExpired() {
        return false;
    }

    @Override
    public void evictAll() {

    }
}
