package com.example.incode13.datingapp.resetpassword;

import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.data.UserRepository;
import com.example.incode13.datingapp.utils.ValidatorUtil;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public class ResetPasswordPresenter implements ResetPasswordContract.Presenter {

    private ResetPasswordContract.View mView;
    private UserRepository mRepository;
    private CompositeSubscription mSubscriptions;

    @Inject
    public ResetPasswordPresenter(ResetPasswordContract.View view, UserRepository repository) {
        mView = view;
        mRepository = repository;
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void resetPassword(String email) {
        mView.removeErrors();

        if (ValidatorUtil.isValidEmail(email) &&
                !ValidatorUtil.isEmpty(email)) {
            reset(email);
        } else {
            mView.setEmailError(mView.context().getString(R.string.error_invalid_email));
        }
    }

    @Override
    public void cancelProgress() {
        unsubscribe();
    }

    private void reset(String email){
        mRepository.resetPassword(email)
                .subscribe(response -> {
                });
    }

    @Override
    public void unsubscribe() {
        if (!mSubscriptions.isUnsubscribed()) {
            mSubscriptions.unsubscribe();
        }
    }
}
