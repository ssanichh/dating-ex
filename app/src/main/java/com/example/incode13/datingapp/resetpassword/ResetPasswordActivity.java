package com.example.incode13.datingapp.resetpassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.common.ToolbarFragmentActivity;
import com.example.incode13.datingapp.utils.ActivityUtils;

public class ResetPasswordActivity extends ToolbarFragmentActivity {

    private static final String EXTRA_EMAIL = "email";

    public static Intent newIntent(Context packageContext, String email) {
        Intent intent = new Intent(packageContext, ResetPasswordActivity.class);
        intent.putExtra(EXTRA_EMAIL, email);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String email = getIntent().getStringExtra(EXTRA_EMAIL);

        ResetPasswordFragment fragment = (ResetPasswordFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container);
        if (fragment == null) {
            fragment = ResetPasswordFragment.newInstance(email);

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.container);
        }

        getSupportActionBar().setTitle(R.string.reset_password_toolbar_title);
    }
}
