
package com.example.incode13.datingapp.data.entity.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("user")
    @Expose
    private User user;
    @Expose(serialize = false)
    @SerializedName("photo")
    private String photo;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("body_type")
    @Expose
    private String bodyType;
    @SerializedName("additional")
    @Expose
    private String additional;
    @SerializedName("alco")
    @Expose
    private String alco;
    @SerializedName("drugs")
    @Expose
    private String drugs;
    @SerializedName("smoking")
    @Expose
    private String smoking;
    @SerializedName("religion")
    @Expose
    private String religion;
    @SerializedName("education")
    @Expose
    private String education;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getAlco() {
        return alco;
    }

    public void setAlco(String alco) {
        this.alco = alco;
    }

    public String getDrugs() {
        return drugs;
    }

    public void setDrugs(String drugs) {
        this.drugs = drugs;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();

        sb.append("****UserInfo****" + "\n");
        sb.append("user=").append(user);
        sb.append("photo=").append(photo).append('\n');
        sb.append("height=").append(height);
        sb.append("bodyType=").append(bodyType).append('\n');
        sb.append("additional=").append(additional).append('\n');
        sb.append("alco=").append(alco).append('\n');
        sb.append("drugs=").append(drugs).append('\n');
        sb.append("smoking=").append(smoking).append('\n');
        sb.append("religion=").append(religion).append('\n');
        sb.append("education=").append(education).append('\n');
        sb.append("*********");
        return sb.toString();
    }
}
