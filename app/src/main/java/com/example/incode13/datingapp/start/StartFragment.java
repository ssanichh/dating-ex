package com.example.incode13.datingapp.start;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.login.LoginActivity;
import com.example.incode13.datingapp.joingender.JoinGenderActivity;
import com.example.incode13.datingapp.common.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartFragment extends BaseFragment implements StartContract.View {

    private static final String TAG = StartFragment.class.getSimpleName();

    @BindView(R.id.signInButton)
    Button mSingnInButton;
    @BindView(R.id.joinButton)
    Button mJoinButton;

    @Inject
    StartPresenter mPresenter;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerStartComponent.builder()
                .startModule(new StartModule(this))
                .build()
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_start, container, false);
        ButterKnife.bind(this, v);

        mSingnInButton.setOnClickListener(view -> mPresenter.singIn());
        mJoinButton.setOnClickListener(view -> mPresenter.signUp());
        return v;
    }

    @Override
    public void onPause() {
        super.onPause();

        mPresenter.unsubscribe();
    }

    @Override
    public void navigateToSingIn() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToSignUp() {
        Intent intent = JoinGenderActivity.newIntent(getActivity());
        startActivity(intent);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }
}
