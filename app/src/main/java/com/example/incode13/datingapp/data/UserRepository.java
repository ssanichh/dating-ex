package com.example.incode13.datingapp.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.incode13.datingapp.data.entity.local.UserData;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Response;
import rx.Observable;

@Singleton
public class UserRepository implements UserDataSource {

    @NonNull
    private final UserDataSource mUserRemoteDataSource;
    @NonNull
    private final UserDataSource mUserLocalDataSource;
    private final Context mContext;

    @Inject
    UserRepository(Context context, @NonNull @Remote UserDataSource remoteUserDataSource,
                   @NonNull @Local UserDataSource localUserDataSource) {
        mUserRemoteDataSource = remoteUserDataSource;
        mUserLocalDataSource = localUserDataSource;
        mContext = context;
    }

    @Override
    public Observable<UserData> getUserData(@NonNull String login, @NonNull String password) {
        return mUserRemoteDataSource.getUserData(login, password);
    }

    @Override
    public Observable<Response> resetPassword(String email) {
        return mUserRemoteDataSource.resetPassword(email);
    }

    @Override
    public Observable<UserData> register(UserData user) {
        return mUserRemoteDataSource.register(user);
    }

    @Override
    public Observable<List<UserData>> getUsers() {
        return mUserRemoteDataSource.getUsers();
    }
}
