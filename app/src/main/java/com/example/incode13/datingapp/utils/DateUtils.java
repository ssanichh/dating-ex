package com.example.incode13.datingapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final String DATE_FORMAT_PATTERN_DB = "yyyy-MM-dd";
    private static final SimpleDateFormat mDateFormatter = new SimpleDateFormat(DATE_FORMAT_PATTERN_DB, Locale.US);

    public static String getFormattedDate(long dateInMillis) {
        return mDateFormatter.format(new Date(dateInMillis));
    }
}
