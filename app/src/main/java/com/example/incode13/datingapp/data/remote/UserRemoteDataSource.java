package com.example.incode13.datingapp.data.remote;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.example.incode13.datingapp.data.UserDataSource;
import com.example.incode13.datingapp.data.auth.Auth;
import com.example.incode13.datingapp.data.cache.UserCache;
import com.example.incode13.datingapp.data.entity.local.UserData;
import com.example.incode13.datingapp.data.entity.mapper.UserDataMapper;
import com.example.incode13.datingapp.data.rest.DatingApi;
import com.example.incode13.datingapp.exception.NetworkConnectionException;
import com.example.incode13.datingapp.utils.NetworkConnectionChecker;

import java.net.SocketTimeoutException;
import java.util.List;

import okhttp3.Response;
import rx.Observable;
import rx.functions.Action1;

public class UserRemoteDataSource implements UserDataSource {

    private static final String TAG = UserRemoteDataSource.class.getSimpleName();
    private static final int MAX_ATTEMPTS = 1;

    private UserDataMapper mDataMapper;
    private UserCache mCache;
    private Context mContext;
    private Auth mAuth;
    private DatingApi mService;

    private final Action1<UserData> saveToCacheAction = userEntity -> {
        if (userEntity != null) {
            UserRemoteDataSource.this.mCache.put(userEntity);
        }
    };

    public UserRemoteDataSource(Context context, UserDataMapper mapper,
                                UserCache cache, Auth auth, DatingApi service) {
        mDataMapper = mapper;
        mCache = cache;
        mContext = context;
        mAuth = auth;
        mService = service;
    }

    @Override
    public Observable<UserData> getUserData(@NonNull String login, @NonNull String password) {
        if (NetworkConnectionChecker.isConnected(mContext)) {
            String credentials = login + ":" + password;
            final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            return mService
                    .login(basic)
                    .map(response -> {
                            mAuth.set(Auth.ACCESS_TOKEN, response.getAccessToken());
                            mAuth.set(Auth.REFRESH_TOKEN, response.getRefreshToken());
                        return mDataMapper.transform(response);
                    })
                    .doOnNext(saveToCacheAction)
                    .retry((attempts, error) ->
                            error instanceof SocketTimeoutException &&
                                    attempts < MAX_ATTEMPTS);
        } else {
            return Observable.error(new NetworkConnectionException());
        }
    }

    @Override
    public Observable<Response> resetPassword(String email) {
        if (NetworkConnectionChecker.isConnected(mContext)) {
            Log.i(TAG, "Sending fake request to the server...");
            return Observable.empty();
        } else {
            return Observable.error(new NetworkConnectionException());
        }
    }

    @Override
    public Observable<UserData> register(UserData user) {
        if(NetworkConnectionChecker.isConnected(mContext)) {
            return mService.register(mDataMapper.transform(user))
                    .map(response -> mDataMapper.transform(response));
        } else {
            return Observable.error(new NetworkConnectionException());
        }
    }

    @Override
    public Observable<List<UserData>> getUsers() {
        if (NetworkConnectionChecker.isConnected(mContext)) {
            return mService.getUsers(mAuth.get(Auth.ACCESS_TOKEN))
                    .map(response -> mDataMapper.transform(response));
        } else {
            return Observable.error(new NetworkConnectionException());
        }
    }
}
