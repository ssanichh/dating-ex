package com.example.incode13.datingapp.joingender;

import com.example.incode13.datingapp.BaseView;

public interface JoinGenderContract {

    interface View extends BaseView {

        void showJoinInfo();
    }

    interface Presenter {

        void continueRegistration();
    }
}
