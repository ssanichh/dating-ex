package com.example.incode13.datingapp.joininfo;

import android.util.Log;

import com.example.incode13.datingapp.data.UserRepository;
import com.example.incode13.datingapp.data.entity.local.UserData;
import com.example.incode13.datingapp.exception.DefaultErrorBundle;
import com.example.incode13.datingapp.exception.ErrorBundle;
import com.example.incode13.datingapp.exception.ErrorMessageFactory;
import com.example.incode13.datingapp.utils.ValidatorUtil;

import javax.inject.Inject;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.example.incode13.datingapp.utils.ValidatorUtil.isEmpty;
import static com.example.incode13.datingapp.utils.ValidatorUtil.isEqual;
import static com.example.incode13.datingapp.utils.ValidatorUtil.isShort;
import static com.example.incode13.datingapp.utils.ValidatorUtil.isValidEmail;

public class JoinInfoPresenter implements JoinInfoContract.Presenter {

    private final static String TAG = JoinInfoFragment.class.getSimpleName();

    private final UserRepository mRepository;
    private final JoinInfoContract.View mView;

    private CompositeSubscription mSubscription;

    @Inject
    JoinInfoPresenter(UserRepository repository, JoinInfoContract.View view) {
        mRepository = repository;
        mView = view;
        mSubscription = new CompositeSubscription();
    }

    @Override
    public void cancelRegistration() {
        unsubscribe();
    }

    @Override
    public void setDateOfBirth() {
        mView.showDatePicker();
    }

    @Override
    public void join(String username, String email, String birthday, String password,
                     String confirmPassword, String smocking, String alco, String drugs,
                     String edrucation, String bodyType, String religion, String height,
                     String additional, String orientation, String gender) {
        mView.removeErrors();

        if (!ValidatorUtil.isValidName(username)) {
            mView.showUserNameError();
        } else if (!isValidEmail(email)) {
            mView.showEmailError();
        } else if (isEmpty(birthday)) {
            mView.showBirthdayError();
        } else if (isEmpty(password)) {
            mView.showEmptyPasswordError();
        } else if (isShort(password)) {
            mView.showShortPasswordError();
        } else if (!isEqual(password, confirmPassword)) {
            mView.showMatchPasswordsError();
        } else if (isEmpty(height)) {
            mView.showHeightError();
        } else {
            UserData user = new UserData();

//            user.setAdditional("test additional");
//            user.setEducation("Bachelors Degree");
//            user.setReligion("Christianity");
//            user.setHeight(123);
//            user.setSmocking("Positive");
//            user.setAlco("Positive");
//            user.setBirthday("1111-11-11");
//            user.setBodyType("Overweight");
//            user.setDrugs("Positive");
//            user.setEmail("caty@em.er");
//            user.setUsername("cat");
//            user.setPassword("test");
//            user.setOrientation("G");
//            user.setGender("W");

            user.setUsername(username);
            user.setEmail(email);
            user.setBirthday(birthday);
            user.setPassword(password);
            user.setSmocking(smocking);
            user.setAlco(alco);
            user.setDrugs(drugs);
            user.setEducation(edrucation);
            user.setBodyType(bodyType);
            user.setReligion(religion);
            user.setHeight(Integer.parseInt(height));
            user.setAdditional(additional);
            user.setOrientation(orientation);
            user.setGender(gender);

            register(user);
        }
    }

    private void register(UserData user) {
        mView.showLoadingIndicator();
        Subscription subscription = mRepository.register(user)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserData>() {
                    @Override
                    public void onCompleted() {
                        mView.hideLoadingIndicator();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideLoadingIndicator();
                        showError(new DefaultErrorBundle((Exception) e));
                    }

                    @Override
                    public void onNext(UserData data) {
                        Log.d(TAG, "onNext received user: " + data);
                    }
                });

        mSubscription.add(subscription);
    }

    private void showError(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(mView.context(), errorBundle.getException());
        mView.showError(errorMessage);
    }

    @Override
    public void unsubscribe() {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }
}
