package com.example.incode13.datingapp.data.rest;

import com.example.incode13.datingapp.data.entity.remote.BrowseUsers;
import com.example.incode13.datingapp.data.entity.remote.UserInfo;
import com.example.incode13.datingapp.data.entity.remote.UserRemoteData;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

public interface DatingApi {

    @Headers("Accept: application/json")
    @POST("user_login/")
    Observable<UserRemoteData> login(@Header("Authorization") String credentials);

    @POST("users/")
    Observable<UserRemoteData> register(@Body UserInfo userInfo);

    @GET("users/")
    Observable<BrowseUsers> getUsers(@Header("Authorization") String token);

}
