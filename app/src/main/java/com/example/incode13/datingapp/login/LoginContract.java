package com.example.incode13.datingapp.login;

import com.example.incode13.datingapp.BasePresenter;
import com.example.incode13.datingapp.BaseView;
import com.example.incode13.datingapp.data.entity.local.UserData;

interface LoginContract {

    interface View extends BaseView {

        void setPresenter(LoginContract.Presenter presenter);

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showError(String errorMessage);

        void showInvalidUserDataError();

        void showBrowseProfiles(UserData userData);

        void setUsernameError();

        void setPasswordError();

        void removeErrors();

        void showResetPassword(String email);
    }

    interface Presenter extends BasePresenter {

        void login(String user, String password);

        void cancelAuthentication();

        void resetPassword(String email);
    }
}
