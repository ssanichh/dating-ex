package com.example.incode13.datingapp.data.cache;

import com.example.incode13.datingapp.data.entity.local.UserData;

import rx.Observable;

public interface UserCache {

    Observable<UserData> get();

    void put(UserData userData);

    boolean isCached();

    boolean isExpired();

    void evictAll();
}
