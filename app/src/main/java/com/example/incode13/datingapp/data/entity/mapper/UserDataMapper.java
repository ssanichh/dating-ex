package com.example.incode13.datingapp.data.entity.mapper;

import com.example.incode13.datingapp.data.Remote;
import com.example.incode13.datingapp.data.entity.local.UserData;
import com.example.incode13.datingapp.data.entity.remote.BrowseUsers;
import com.example.incode13.datingapp.data.entity.remote.User;
import com.example.incode13.datingapp.data.entity.remote.UserInfo;
import com.example.incode13.datingapp.data.entity.remote.UserRemoteData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@Remote
public class UserDataMapper {

    @Inject
    UserDataMapper() {}

    public UserData transform(UserRemoteData data) {
        UserData userData = null;
        if (data != null) {
            userData = mapUserInfo(data.getUserInfo());
        }
        return userData;
    }

    public UserInfo transform(UserData data) {
        UserInfo userInfo = null;
        if (data != null) {
            User user = new User();
            user.setBirthday(data.getBirthday());
            user.setEmail(data.getEmail());
            user.setGender(data.getGender());
            user.setOrientation(data.getOrientation());
            user.setUsername(data.getUsername());
            user.setPassword(data.getPassword());
            user.setGender(data.getGender());

            userInfo = new UserInfo();
            userInfo.setUser(user);
            userInfo.setAdditional(data.getAdditional());
            userInfo.setAlco(data.getAlco());
            userInfo.setBodyType(data.getBodyType());
            userInfo.setDrugs(data.getDrugs());
            userInfo.setEducation(data.getEducation());
            userInfo.setHeight(data.getHeight());
            userInfo.setReligion(data.getReligion());
            userInfo.setSmoking(data.getSmocking());

        }
        return userInfo;
    }

    public List<UserData> transform(BrowseUsers browseUsers) {
        List<UserInfo> usersInfo = browseUsers.getResults();
        List<UserData> users = new ArrayList<>();
        for (UserInfo u: usersInfo) {
            UserData user = mapUserInfo(u);
            users.add(user);
        }
        return users;
    }

    private UserData mapUserInfo(UserInfo userInfo) {
        UserData userData = new UserData();
        userData.setUsername(userInfo.getUser().getUsername());
        userData.setBirthday(userInfo.getUser().getBirthday());
        userData.setEmail(userInfo.getUser().getEmail());
        userData.setPhotoUrl(userInfo.getPhoto());
        userData.setGender(userInfo.getUser().getGender());
        userData.setOrientation(userInfo.getUser().getOrientation());
        userData.setAdditional(userInfo.getAdditional());
        userData.setAlco(userInfo.getAlco());
        userData.setBodyType(userInfo.getBodyType());
        userData.setHeight(userInfo.getHeight());
        userData.setDrugs(userInfo.getDrugs());
        userData.setSmocking(userInfo.getSmoking());
        userData.setReligion(userInfo.getReligion());
        userData.setEducation(userInfo.getEducation());

        return userData;
    }
}