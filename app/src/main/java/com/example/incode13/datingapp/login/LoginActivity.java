package com.example.incode13.datingapp.login;

import android.os.Bundle;

import com.example.incode13.datingapp.DatingApplication;
import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.common.ToolbarFragmentActivity;
import com.example.incode13.datingapp.utils.ActivityUtils;

import javax.inject.Inject;

public class LoginActivity extends ToolbarFragmentActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    @Inject LoginPresenter mPresenter;

//    private String mName;
//    private String mPassword;
//    private UserApiInterface mUserApi;
//    private FacebookApiInterface mFacebookApi;
//    private ProgressBar mLoadingView;
//    private LoginInterface.UserActionListener mActionListener;

//    @Override
//    protected int getLayoutResId() {
//        return R.layout.fragment_login;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        final TextInputLayout passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
//        final TextInputLayout usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
//        TextView facebookLogin = (TextView) findViewById(R.id.facebookLogin);
//        Button loginButton = (Button) findViewById(R.id.loginButton);
//
//        mActionListener = new LoginImpl(getApplicationContext(), this);
//
//        mLoadingView = new ProgressBar(this);
////        mUserApi = new UserApiManager(this);
//
//        if (passwordWrapper.getEditText() != null) {
//            passwordWrapper.getEditText().setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View view, MotionEvent motionEvent) {
//                    final int DRAWABLE_RIGHT = 2;
//                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                        if (motionEvent.getX() >= (passwordWrapper.getEditText().getWidth() - passwordWrapper.getEditText()
//                                .getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                            Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
//                            startActivity(intent);
//                            return true;
//                        }
//                    }
//                    return false;
//                }
//            });
//        }
//
//        facebookLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (NetworkConnectionChecker.isConnected(getApplicationContext())) {
//                    mFacebookApi = new FacebookApiManager(LoginActivity.this, LoginActivity.this);
//                    mFacebookApi.registerFacebookCallback();
//                }
//            }});
//
//        loginButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (ValidationUtils.validateIsNotEmpty(usernameWrapper) &&
//                        ValidationUtils.validatePasswordLength(passwordWrapper)) {
//                    mName = usernameWrapper.getEditText().getText().toString();
//                    mPassword = passwordWrapper.getEditText().getText().toString();
//                    if (NetworkConnectionChecker.isConnected(LoginActivity.this)) {
//                        CurrentUser user = new CurrentUser();
//                        user.setIdentificator(mName);
//                        user.setPassword(mPassword);
//
////                        mUserApi.login(user);
//                        mActionListener.login(user);
//
//                    }
//                }
//            }
//
//        });

        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container);
        if (loginFragment == null) {
            loginFragment = LoginFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    loginFragment, R.id.container);
        }

        DaggerLoginComponent.builder()
                .userRepositoryComponent(((DatingApplication) getApplication()).getUserRepositoryComponent())
                .loginModule(new LoginModule(loginFragment))
                .build()
                .inject(this);

        getSupportActionBar().setTitle(R.string.login_toolbar_title);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        mFacebookApi.onActivityResult(requestCode, resultCode, data);
//    }

    @Override
    protected void onPause() {
        super.onPause();
    }

//    @Override
//    public void showNextActivity() {
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//    }
//
//    @Override
//    public void showError(String error) {
//        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
//    }

//    @Override
//    public void showIsLoading(boolean isLoading) {
//        if (isLoading) {
//            mLoadingView.showProgress();
//        } else {
//            mLoadingView.cancelProgress();
//        }
//    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//        mActionListener.destroy();
//    }
}

