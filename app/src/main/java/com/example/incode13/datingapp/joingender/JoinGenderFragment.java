package com.example.incode13.datingapp.joingender;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.joininfo.JoinInfoActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.incode13.datingapp.utils.SpinnerUtils.setupSpinner;

public class JoinGenderFragment extends Fragment implements JoinGenderContract.View, AdapterView.OnItemSelectedListener {

    private static final String GENDER_MAN = "M";
    private static final String GENDER_WOMAN = "W";
    private static final String ORIENTATION_STRAIGHT = "S";
    private static final String ORIENTATION_GAY = "G";

    @BindView(R.id.orientationSpinner)
    Spinner mOrientationSpinner;
    @BindView(R.id.genderSpinner)
    Spinner mGenderSpinner;
    @BindView(R.id.continueButton)
    Button mContinueButton;

    @Inject
    JoinGenderPresenter mPresenter;

    private String mOrientation;
    private String mGender;

    public static JoinGenderFragment newInstance() {
        return new JoinGenderFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerJoinGenderComponent.builder()
                .joinGenderModule(new JoinGenderModule(this))
                .build()
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_join_gender, container, false);
        ButterKnife.bind(this, v);

        setupSpinner(mOrientationSpinner, R.array.orientation, this);
        setupSpinner(mGenderSpinner, R.array.gender, this);

        mContinueButton.setOnClickListener(v1 -> mPresenter.continueRegistration());
        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case (R.id.orientationSpinner):
                mOrientation = adapterView.getSelectedItemPosition() == 0 ?
                        ORIENTATION_STRAIGHT : ORIENTATION_GAY;
                break;
            case (R.id.genderSpinner):
                mGender = adapterView.getSelectedItemPosition() == 0 ?
                        GENDER_MAN : GENDER_WOMAN;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void showJoinInfo() {
        Intent intent = JoinInfoActivity.newIntent(getContext(), mGender, mOrientation);
        startActivity(intent);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }
}
