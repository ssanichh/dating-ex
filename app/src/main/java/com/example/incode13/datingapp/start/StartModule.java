package com.example.incode13.datingapp.start;

import dagger.Module;
import dagger.Provides;

@Module
public class StartModule {

    private final StartContract.View mView;

    public StartModule(StartContract.View view) {
        mView = view;
    }

    @Provides
    StartContract.View provideStartContractView() {
        return mView;
    }
}
