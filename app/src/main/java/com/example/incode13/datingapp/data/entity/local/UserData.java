package com.example.incode13.datingapp.data.entity.local;

public class UserData {

    private String mPhotoUrl;
    private String mUsername;
    private String mBirthday;
    private String mLocation;
    private String mEmail;
    private String mGender;
    private String mOrientation;
    private int mHeight;
    private String mBodyType;
    private String mAdditional;
    private String mAlco;
    private String mDrugs;
    private String mSmocking;
    private String mReligion;
    private String mEducation;
    private String mPassword;

    public UserData() {}

    public String getName() {
        return mUsername;
    }

    public void setUsername(String username) {
        this.mUsername = username;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String birthday) {
        this.mBirthday = birthday;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        this.mLocation = location;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        this.mGender = gender;
    }

    public String getOrientation() {
        return mOrientation;
    }

    public void setOrientation(String orientation) {
        this.mOrientation = orientation;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.mPhotoUrl = photoUrl;
    }

    public String getUsername() {
        return mUsername;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public String getBodyType() {
        return mBodyType;
    }

    public void setBodyType(String bodyType) {
        mBodyType = bodyType;
    }

    public String getAdditional() {
        return mAdditional;
    }

    public void setAdditional(String additional) {
        mAdditional = additional;
    }

    public String getAlco() {
        return mAlco;
    }

    public void setAlco(String alco) {
        mAlco = alco;
    }

    public String getDrugs() {
        return mDrugs;
    }

    public void setDrugs(String drugs) {
        mDrugs = drugs;
    }

    public String getSmocking() {
        return mSmocking;
    }

    public void setSmocking(String smocking) {
        mSmocking = smocking;
    }

    public String getReligion() {
        return mReligion;
    }

    public void setReligion(String religion) {
        mReligion = religion;
    }

    public String getEducation() {
        return mEducation;
    }

    public void setEducation(String education) {
        mEducation = education;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();

        sb.append("****UserRemoteData****" + "\n");
        sb.append("mPhotoUrl=").append(mPhotoUrl).append('\n');
        sb.append("mUsername=").append(mUsername).append('\n');
        sb.append("mBirthday=").append(mBirthday).append('\n');
        sb.append("mLocation=").append(mLocation).append('\n');
        sb.append("mEmail=").append(mEmail).append('\n');
        sb.append("mGender=").append(mGender).append('\n');
        sb.append("mOrientation=").append(mOrientation).append('\n');
        sb.append("mHeight=").append(mHeight);
        sb.append("mBodyType=").append(mBodyType).append('\n');
        sb.append("mAdditional=").append(mAdditional).append('\n');
        sb.append("mAlco=").append(mAlco).append('\n');
        sb.append("mDrugs=").append(mDrugs).append('\n');
        sb.append("mSmocking=").append(mSmocking).append('\n');
        sb.append("mReligion=").append(mReligion).append('\n');
        sb.append("mEducation=").append(mEducation).append('\n');
        sb.append("*********");
        return sb.toString();
    }
}
