package com.example.incode13.datingapp.joininfo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.incode13.datingapp.R;
import com.example.incode13.datingapp.common.ToolbarFragmentActivity;
import com.example.incode13.datingapp.utils.ActivityUtils;

public class JoinInfoActivity extends ToolbarFragmentActivity {

    private static final String EXTRA_GENDER = "gender";
    private static final String EXTRA_ORIENTATION = "orientation";

    public static Intent newIntent(Context packageContext, String gender, String orientation) {
        Intent intent = new Intent(packageContext, JoinInfoActivity.class);
        intent.putExtra(EXTRA_GENDER, gender);
        intent.putExtra(EXTRA_ORIENTATION, orientation);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JoinInfoFragment fragment = (JoinInfoFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container);
        if (fragment == null) {
            String gender = getIntent().getStringExtra(EXTRA_GENDER);
            String orientation = getIntent().getStringExtra(EXTRA_ORIENTATION);

            fragment = JoinInfoFragment.newInstance(gender, orientation);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.container);
        }

        getSupportActionBar().setTitle(R.string.join_gender_toolbar_title);
    }
}
