package com.example.incode13.datingapp.utils;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.incode13.datingapp.R;

public class SpinnerUtils {

    public static void setupSpinner(Spinner spinner, int array, AdapterView.OnItemSelectedListener listener) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(spinner.getContext(),
                R.layout.list_item_spinner,
                spinner.getContext().getResources().getStringArray(array));

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(listener);
    }
}
