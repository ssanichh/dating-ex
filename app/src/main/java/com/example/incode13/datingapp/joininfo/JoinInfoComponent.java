package com.example.incode13.datingapp.joininfo;

import com.example.incode13.datingapp.data.UserRepositoryComponent;
import com.example.incode13.datingapp.utils.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = UserRepositoryComponent.class, modules = JoinInfoModule.class)
public interface JoinInfoComponent {

    void inject(JoinInfoFragment fragment);
}
