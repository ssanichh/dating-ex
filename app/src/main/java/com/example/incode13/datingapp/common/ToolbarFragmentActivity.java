package com.example.incode13.datingapp.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.example.incode13.datingapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ToolbarFragmentActivity extends SingleFragmentActivity {

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_toolbar_fragment;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mToolbar.setNavigationOnClickListener(view -> onBackPressed());
    }
}
