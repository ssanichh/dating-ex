package com.example.incode13.datingapp.data.local;

import android.support.annotation.NonNull;

import com.example.incode13.datingapp.data.UserDataSource;
import com.example.incode13.datingapp.data.cache.UserCache;
import com.example.incode13.datingapp.data.entity.local.UserData;

import java.util.List;

import okhttp3.Response;
import rx.Observable;

public class UserLocalDataSource implements UserDataSource {

    private UserCache mCache;

    public UserLocalDataSource(UserCache cache) {
        mCache = cache;
    }

    @Override
    public Observable<UserData> getUserData(@NonNull String login, @NonNull String password) {
        return null;
    }

    @Override
    public Observable<Response> resetPassword(String email) {
        return null;
    }

    @Override
    public Observable<UserData> register(UserData user) {
        return null;
    }

    @Override
    public Observable<List<UserData>> getUsers() {
        return null;
    }
}
